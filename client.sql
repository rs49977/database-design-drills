
CREATE TABLE Client
(
    Client_id INT AUTO_INCREMENT,
    Client_name VARCHAR(100),
    Client_Loc VARCHAR(200),
    PRIMARY KEY(Client_id)
);

CREATE TABLE Manager
(
    Manager_id INT AUTO_INCREMENT,
    Manager_name VARCHAR(100) NOT NULL,
    Manager_loc VARCHAR(200) NOT NULL,
    PRIMARY KEY(Manager_id) 
);

CREATE TABLE Client_Manager
(
    Client_id INT NOT NULL,
    Manager_id INT NOT NULL,
    PRIMARY KEY(Client_id, Manager_id),
    FOREIGN KEY(Client_id) REFERENCES Client(Client_id),
    FOREIGN KEY(Manager_id) REFERENCES Manager(Manager_id)
);


CREATE TABLE Contract
(
    Contract_id INT AUTO_INCREMENT,
    Manager_id INT,
    Estimated_cost INT Unsigned,
    Completion_date DATE,
    PRIMARY KEY(Contract_id),
    FOREIGN KEY(Manager_id) REFERENCES Manager(Manager_id)
);

CREATE TABLE Staff
(
    Staff_id INT AUTO_INCREMENT,
    Manager_id INT,
    Name VARCHAR(100) NOT NULL,
    Location VARCHAR(200),
    PRIMARY KEY(Staff_id),
    FOREIGN KEY(Manager_id) REFERENCES Manager(Manager_id)
);
