CREATE TABLE Patient
(
    Pt_id INT AUTO_INCREMENT,
    Pt_name VARCHAR(100) NOT NULL,
    Pt_add VARCHAR(200) NOT NULL,
    Pt_DOB Date NOT NULL,
    PRIMARY KEY(Pt_id)
);

CREATE TABLE Prescription
(
    Pr_id INT AUTO_INCREMENT,
    Drug_id INT NOT NULL,
    Pt_id INT,
    Dr_id INT,
    Drug_dose TINYINT Unsigned,
    Drug_date DATE,
    PRIMARY KEY(Pr_id),
    FOREIGN KEY(Pt_id) REFERENCES Patient(Pt_id),
    FOREIGN KEY(Dr_id) REFERENCES Doctor(Dr_id)
);

CREATE TABLE Doctor
(
    Dr_id INT AUTO_INCREMENT,
    Dr_name VARCHAR(100) NOT NULL,
    Sec_name VARCHAR(100),
    PRIMARY KEY(Dr_id)
);
