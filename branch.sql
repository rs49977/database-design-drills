CREATE TABLE Branch_info
(
    Branch_id INT AUTO_INCREMENT,
    Branch_addr VARCHAR(200),
    PRIMARY KEY(Branch_id)
);

CREATE TABLE Book_info
(
    Book_id INT AUTO_INCREMENT,
    Branch_id INT,
    Book_title VARCHAR(100) NOT NULL,
    Book_auther VARCHAR(100) NOT NULL,
    Book_ISBN CHAR(10),
    Num_copies INT Unsigned,
    PRIMARY KEY(Book_id),
    FOREIGN KEY(Branch_id) REFERENCES Branch_info(Branch_id) 
);


