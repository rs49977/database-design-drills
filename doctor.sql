CREATE TABLE Doctor
(
    Dr_id INT AUTO_INCREMENT,
    Dr_name VARCHAR(100) NOT NULL,
    Sec_name VARCHAR(100),
    PRIMARY KEY(Dr_id
);

CREATE TABLE Patient
(
    Pt_id INT AUTO_INCREMENT,
    Pt_name VARCHAR(100) NOT NULL,
    Pt_add VARCHAR(200) NOT NULL,
    Pt_DOB Date NOT NULL,
    PRIMARY KEY(Pt_id)
);

CREATE TABLE Doctor_patient
(
    Dr_id INT NOT NULL,
    Pt_id INT NOT NULL,
    PRIMARY KEY(Dr_id, Pt_id),
    FOREIGN KEY(Dr_id) REFERENCES Doctor(Dr_id),
    FOREIGN KEY(Pt_id) REFERENCES Patient(Pt_id)
);

CREATE TABLE Drug
(
    Drug_id INT AUTO_INCREMENT,
    Drug_name VARCHAR(100) NOT NULL,
    Drug_dose TINYINT Unsigned,
    Drug_date DATE,
    PRIMARY KEY(Drug_id)
);

CREATE TABLE Patient_drug 
(
    Pt_id INT NOT NULL,
    Drug_id INT NOT NULL,
    PRIMARY KEY(Pt_id, Drug_id),
    FOREIGN KEY(Pt_id) REFERENCES Patient(Pt_id),
    FOREIGN KEY(Drug_id) REFERENCES Drug(Drug_id)
);
